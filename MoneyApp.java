class Main {
    public static void main(String[] args) {
     
    }
    public double findIntrestRate(double balance, int years, double principal){
      return (Math.pow(balance/principal,1/years)-1);
    }
    public double findYears(double balance, double rate, double principal){
      return (log(balance/principal)/log(1+rate));
    }
  }
class Account{
  double balance = 0.0;
  double rate = 0.0;
  int yearsActive = 0;
  public Account(double principal,double rate){
    self.balance = principal;
    self.rate = rate;
    self.yearsActive = 0;
  }
  public Account(double principal,double rate, int years){
    self.balance = principal*(Math.pow(1+rate,years));
    self.rate = rate;
    self.yearsActive = years;
  }
  
}
